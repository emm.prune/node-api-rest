const express = require('express');
const cors = require('cors');
const app = express();
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const mongoose = require('mongoose');

var mongoDB = 'mongodb://localhost:27017/testDB';
mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true},	function(err, res){
	if (err) throw err;
	console.log('Connected to Database');
});
mongoose.Promise = global.Promise;
var db = mongoose.connection;

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json());
app.use(methodOverride());

var models = require('./models/tasklist')(app, mongoose);
const TaskListCtrl = require('./controllers/tasklist');

/*var router = express.Router();
router.get('/', function(req, res) {
	res.send("Hola Mundo!");
});
app.use(router);*/

var tasklist = express.Router();

tasklist.route('/tasklist')
	.get(TaskListCtrl.findAll)
	.post(TaskListCtrl.addField);

tasklist.route('/tasklist/:id')
	.get(TaskListCtrl.findById)
	.put(TaskListCtrl.updateField)
	.delete(TaskListCtrl.deleteField);

//app.use(router);
app.use('/api', tasklist);

app.listen(3000, function() {
	console.log("Node server running on http://localhost:3000");
});