exports = module.exports = function(app, mongoose) {

	var Schema = mongoose.Schema;

	var tasklistSchema =new Schema({
		id: Number,
		name: String
	});

	mongoose.model('TaskList', tasklistSchema);

};