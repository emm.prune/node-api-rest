const mongoose = require('mongoose');
var TaskList = mongoose.model('TaskList');

exports.findAll = function(req, res) {
	TaskList.find(function(err, tasklist) {
		if (err) res.send(500, err.message);

		var myArray = tasklist;
		for (let index = 0; index < tasklist.length; ++index) {
		    let myJson = {};
		    myJson._id = tasklist[index]._id;
		    myJson.id = tasklist[index].id;
		    myJson.name = tasklist[index].name;
		    myArray[index] = myJson;
		}

		console.log('GET /tasklist/findAll');
		res.status(200).jsonp(tasklist);
	})
};

exports.findById = function(req, res) {
	TaskList.findOne({"id": req.params.id}, function(err, tasklist) {
		if (err) return res.send(500, err.message);

		console.log('GET /tasklist/findById/' + req.params.id);
		res.status(200).jsonp(tasklist);
	});
};

exports.addField = function(req, res) {
	console.log('POST');
	console.log(req.body);

	TaskList.findOne({},null,{sort: {_id: -1}}, function(err, lasttask) {
		var tasklist = new TaskList({
			name: req.body.name,
			id: lasttask.id + 1
		});

		tasklist.save(function(err, tasklist) {
			if (err) return res.send(500, err.message);

			res.status(200).jsonp(tasklist);
		});
	});
};

exports.updateField = function(req, res) {
	TaskList.findOne({"id": req.params.id}, function(err, tasklist) {
		tasklist.name = req.body.name;

		tasklist.save(function(err) {
			if (err) return res.send(500, err.message);
			res.status(200).jsonp(tasklist);
		});
	});
};

exports.deleteField = function(req, res) {
	TaskList.findOne({"id": req.params.id}, function(err, tasklist) {
		tasklist.remove(function(err) {
			if (err) return res.send(500, err.message);
			res.status(200).jsonp(tasklist);
		});
	});
};